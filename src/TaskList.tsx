import React, { useState } from "react";
import ITask from "./Interfaces";
export interface IProps {
  tasks: ITask[];
  deleteTask: (task: ITask) => void;
  updateTask: (task: ITask) => void;
}

function TaskList(props: IProps) {
  const [formValue, setFormValue] = useState({ email: "", password: "" });

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });
  };

  return (
    <table className="demTable" border={1} width="100%">
      <thead>
        <tr>
          <td colSpan={4}>
            <label>
              E-Mail:{" "}
              <input onChange={onInputChange} id="email" name="email"></input>
            </label>

            <label>
              {" "}
              Passwort:{" "}
              <input
                onChange={onInputChange}
                id="password"
                name="password"
              ></input>
            </label>
            <button> Log In</button>
          </td>
        </tr>
        <tr>
          <td colSpan={4} className="FATTEST">
            Aufgaben Liste:
          </td>
        </tr>
        <tr>
          <td id="theader">ID</td>
          <td id="theader" width="50%">
            Title
          </td>
          <td id="theader">Completed</td>
          <td id="theader">Operations</td>
        </tr>
      </thead>
      <tbody>
        {props.tasks.map((task) => {
          let tdId = `${task.id}`;

          return (
            <tr key={task.id}>
              <td>{task.id}</td>
              <td id={tdId}>{task.title}</td>
              <td>{task.completed}</td>
              <td>
                <button onClick={() => props.deleteTask(task)}>Löschen</button>
                <button onClick={() => props.updateTask(task)}>Update</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default TaskList;
