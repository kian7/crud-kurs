

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Git Log
PS C:\Users\kian.schuele\Desktop\crud gitlab\crud> git log
commit dc0fd68ad57a55c23fc456e04046e3ea8fe75ced (HEAD -> master)      
Author: Kian <kian@schuele.org>
Date:   Wed Aug 31 15:38:39 2022 +0200

    alles ka

commit 79dfde785eb5e707f8161a5040248cce11561c86
Author: Kian <kian@schuele.org>
Date:   Tue Aug 30 19:18:02 2022 +0200

    Added new things (before table implement)

commit 0ace63432320a8955fb0b749f02116e062791d96 (origin/master)       
Author: Kian <kian@schuele.org>
Date:   Tue Aug 30 11:00:46 2022 +0200

    Add Delete button function
:...skipping...
commit dc0fd68ad57a55c23fc456e04046e3ea8fe75ced (HEAD -> master)
Author: Kian <kian@schuele.org>
Date:   Wed Aug 31 15:38:39 2022 +0200

    alles ka

commit 79dfde785eb5e707f8161a5040248cce11561c86
Author: Kian <kian@schuele.org>
Date:   Tue Aug 30 19:18:02 2022 +0200

    Added new things (before table implement)

commit 0ace63432320a8955fb0b749f02116e062791d96 (origin/master)
Author: Kian <kian@schuele.org>
Date:   Tue Aug 30 11:00:46 2022 +0200

    Add Delete button function

commit c2ad1cee7c29a0d8b031219a03dbffcb4a3b6718
Author: Florian Bosshard <florian.bosshard@gmail.com>
Date:   Mon Aug 29 20:15:04 2022 +0200

    List and Create functionality

commit c072791772db3fbc0ce9941aa8441f7448e5202c
Author: Florian Bosshard <florian.bosshard@gmail.com>
Date:   Mon Aug 29 17:38:51 2022 +0200

    Initialize project using Create React App
:...skipping...
commit dc0fd68ad57a55c23fc456e04046e3ea8fe75ced (HEAD -> master)
Author: Kian <kian@schuele.org>
Date:   Wed Aug 31 15:38:39 2022 +0200

    alles ka

commit 79dfde785eb5e707f8161a5040248cce11561c86
Author: Kian <kian@schuele.org>
Date:   Tue Aug 30 19:18:02 2022 +0200

    Added new things (before table implement)

commit 0ace63432320a8955fb0b749f02116e062791d96 (origin/master)
Author: Kian <kian@schuele.org>
Date:   Tue Aug 30 11:00:46 2022 +0200

    Add Delete button function

commit c2ad1cee7c29a0d8b031219a03dbffcb4a3b6718
Author: Florian Bosshard <florian.bosshard@gmail.com>
Date:   Mon Aug 29 20:15:04 2022 +0200

    List and Create functionality

commit c072791772db3fbc0ce9941aa8441f7448e5202c
Author: Florian Bosshard <florian.bosshard@gmail.com>
Date:   Mon Aug 29 17:38:51 2022 +0200

    Initialize project using Create React App
~
(END)


## Git Repo

https://gitlab.com/kian7/crud-kurs